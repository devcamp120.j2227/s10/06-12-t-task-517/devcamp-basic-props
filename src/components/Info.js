import { Component } from "react";

class Info extends Component {
    render() {
        console.log(this.props);
        const {firstname, lastname, number} = this.props;
        //this.props.firstname = "Nguyen";
        return (
            <div>
                <p>My name is {firstname} {lastname} and my favourite number is {number}</p>
            </div>
        )
    }
}

export default Info